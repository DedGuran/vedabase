
import { Box, Flex, Text } from '@chakra-ui/layout'
import { Image } from "@chakra-ui/react"
import React from 'react'
import GradientLayout from '../components/gradientLayout'
import { useMe } from '../lib/hooks'
import prisma from '../lib/prisma'

const Home = ({ categories }) => {
  
  const { user, isLoading } = useMe()

  return (
    <GradientLayout roundImage color="blue" subtitle="profile" title={`${user?.firstName} ${user?.lastName}`} description={`${user?.questionCount} public questions`}>
      <Box color="white">
        <Box>
          <Text fontSize="2xl">Все категории</Text>
          <Text fontSize="md">Видны только тебе</Text>
        </Box>
        
       
        <Flex>{categories.map((categorie) => {
          return(
          <Box paddingX="10px" width="20%">
            <Box bg="gray.800" borderRadius="4px" padding="15px" width="100%">
              <Image src="https://images.theconversation.com/files/443350/original/file-20220131-15-1ndq1m6.jpg?ixlib=rb-1.1.0&rect=0%2C0%2C3354%2C2464&q=45&auto=format&w=926&fit=clip" />

              <Box marginTop="20px">
                <Text fontSize="large">{categorie.name}</Text>
                <Text fontSize="x-small">Artict</Text>
              </Box>
            </Box>
          </Box>
          )
        })}</Flex>

      </Box>
    </GradientLayout>


  )
}
export const getServerSideProps = async () => {
  const categories = await prisma.categories.findMany({})

  return {
    props: {
      categories: JSON.parse(JSON.stringify(categories))
    },
  }
}

export default Home