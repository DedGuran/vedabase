import AnswerTable from "../../components/answersTable"
import GradientLayout from "../../components/gradientLayout"
import { validateToken } from "../../lib/auth"
import prisma from "../../lib/prisma"

const getBGColor = (id) => {
    const colors = [
        "red",
        "green",
        "blue",
        "orange",
        "purple",
        "gray",
        "teal",
        "yellow"
    ]

    return colors[id - 1] || colors[Math.floor(Math.random() * colors.length)]
}

const Questions = ({ questions }) => {
  
    const color = getBGColor(questions.id)
    return (
        <GradientLayout color={color}
                        roundImage={false}
                        title={questions.name} 
                        subtitle="вопрос" 
                        description={`${questions.answer.length} ответа`} 
                        image={`https://picsum.photos/400?random=${questions.id}`}>
            <AnswerTable answers={questions.answer}/>
        </GradientLayout>
    )
}

export const getServerSideProps = async ({ query, req, }) => {
    let user 
    try {
        user=validateToken(req.cookies.TRAX_ACCESS_TOKEN)
    } catch (e) {
        return{
            permanent:false,
            destination:"/signin"
        }
    }
   // const { id } = validateToken(req.cookies.TRAX_ACCESS_TOKEN)
    const [questions] = await prisma.question.findMany({
        where: {
            id: +query.id,
            userId: user.id,
        },
        include: {
            answer: {
                include: {
                    categorie: {
                        select: {
                            name: true,
                            id: true
                        }
                    }
                }
            }
        }
    })

    return {
        props: { questions: JSON.parse(JSON.stringify(questions)) }
    }
}

export default Questions