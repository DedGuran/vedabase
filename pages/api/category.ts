import prisma from "../../lib/prisma";
import { validateRoute } from "../../lib/auth";

export default validateRoute(async(req,res)=>{
    const categories=await prisma.categories.findMany({})
    res.json(categories)
}) 