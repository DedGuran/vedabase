import prisma from "../../lib/prisma";
import { validateRoute } from "../../lib/auth";

export default validateRoute(async(req,res,user)=>{
    const questions=await prisma.question.findMany({
        where:{
            userId:user.id
        },
        orderBy:{
            name:'asc'
        }
    })
    res.json(questions)
}) 