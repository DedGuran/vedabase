import { validateRoute } from "../../lib/auth";
import prisma from "../../lib/prisma";
export default validateRoute (async(req,res,user)=>{
    const questionCount= await prisma.question.count({
        where:{
            userId:user.id
        }
    })
   
    res.json({...user,questionCount})
})