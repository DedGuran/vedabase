import type { NextRequest } from "next/server";
import { NextResponse } from "next/server"


const signedinPages=["/","/question","/library"]


export default function middleware(req:NextRequest){
    const hostname = req.headers.get('host')
    
    if(signedinPages.find((p)=>p===req.nextUrl.pathname)){
        const token =req.cookies.TRAX_ACCESS_TOKEN

        if(!token){
            if(hostname === 'localhost:3000'){
                return NextResponse.redirect(`http://${hostname}/signin`)
            }else{
                return NextResponse.redirect(`https://${hostname}/signin`)
            }
            
            //console.log("err")
        }
    }
}



