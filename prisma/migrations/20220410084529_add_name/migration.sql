/*
  Warnings:

  - A unique constraint covering the columns `[name]` on the table `Answer` will be added. If there are existing duplicate values, this will fail.
  - A unique constraint covering the columns `[name]` on the table `Question` will be added. If there are existing duplicate values, this will fail.
  - Added the required column `duration` to the `Answer` table without a default value. This is not possible if the table is not empty.
  - Added the required column `name` to the `Answer` table without a default value. This is not possible if the table is not empty.
  - Added the required column `url` to the `Answer` table without a default value. This is not possible if the table is not empty.
  - Added the required column `name` to the `Question` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "Answer" ADD COLUMN     "duration" INTEGER NOT NULL,
ADD COLUMN     "name" TEXT NOT NULL,
ADD COLUMN     "url" TEXT NOT NULL;

-- AlterTable
ALTER TABLE "Question" ADD COLUMN     "name" TEXT NOT NULL;

-- CreateIndex
CREATE UNIQUE INDEX "Answer_name_key" ON "Answer"("name");

-- CreateIndex
CREATE UNIQUE INDEX "Question_name_key" ON "Question"("name");
