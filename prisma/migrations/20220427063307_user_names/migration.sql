/*
  Warnings:

  - You are about to alter the column `id` on the `Question` table. The data in that column will be cast from `BigInt` to `Int`. This cast may fail. Please make sure the data in the column can be cast.
  - You are about to alter the column `userId` on the `Question` table. The data in that column could be lost. The data in that column will be cast from `BigInt` to `Int`.
  - You are about to alter the column `categorieId` on the `Answer` table. The data in that column could be lost. The data in that column will be cast from `BigInt` to `Int`.
  - You are about to alter the column `duration` on the `Answer` table. The data in that column could be lost. The data in that column will be cast from `BigInt` to `Int`.
  - You are about to alter the column `id` on the `Answer` table. The data in that column will be cast from `BigInt` to `Int`. This cast may fail. Please make sure the data in the column can be cast.
  - You are about to alter the column `id` on the `User` table. The data in that column will be cast from `BigInt` to `Int`. This cast may fail. Please make sure the data in the column can be cast.
  - You are about to alter the column `id` on the `Categories` table. The data in that column will be cast from `BigInt` to `Int`. This cast may fail. Please make sure the data in the column can be cast.
  - Changed the type of `A` on the `_AnswerToQuestion` table. No cast exists, the column would be dropped and recreated, which cannot be done if there is data, since the column is required.
  - Changed the type of `B` on the `_AnswerToQuestion` table. No cast exists, the column would be dropped and recreated, which cannot be done if there is data, since the column is required.
  - Added the required column `description` to the `Answer` table without a default value. This is not possible if the table is not empty.

*/
-- DropForeignKey
ALTER TABLE "_AnswerToQuestion" DROP CONSTRAINT "fk_A_ref_Answer";

-- DropForeignKey
ALTER TABLE "_AnswerToQuestion" DROP CONSTRAINT "fk_B_ref_Question";

-- AlterTable
ALTER TABLE "_AnswerToQuestion" DROP COLUMN "A";
ALTER TABLE "_AnswerToQuestion" ADD COLUMN     "A" INT4 NOT NULL;
ALTER TABLE "_AnswerToQuestion" DROP COLUMN "B";
ALTER TABLE "_AnswerToQuestion" ADD COLUMN     "B" INT4 NOT NULL;

-- RedefineTables
CREATE TABLE "_prisma_new_Question" (
    "id" SERIAL4 NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updateAt" TIMESTAMP(3) NOT NULL,
    "name" TEXT NOT NULL,
    "userId" INT4 NOT NULL,

    CONSTRAINT "Question_pkey" PRIMARY KEY ("id")
);
DROP INDEX "Question_name_key";
INSERT INTO "_prisma_new_Question" ("createdAt","id","name","updateAt","userId") SELECT "createdAt","id","name","updateAt","userId" FROM "Question";
DROP TABLE "Question" CASCADE;
ALTER TABLE "_prisma_new_Question" RENAME TO "Question";
CREATE UNIQUE INDEX "Question_name_key" ON "Question"("name");
ALTER TABLE "Question" ADD CONSTRAINT "Question_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
CREATE TABLE "_prisma_new_Answer" (
    "id" SERIAL4 NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updateAt" TIMESTAMP(3) NOT NULL,
    "name" TEXT NOT NULL,
    "description" TEXT NOT NULL,
    "categorieId" INT4 NOT NULL,
    "duration" INT4 NOT NULL,
    "url" TEXT NOT NULL,

    CONSTRAINT "Answer_pkey" PRIMARY KEY ("id")
);
DROP INDEX "Answer_name_key";
INSERT INTO "_prisma_new_Answer" ("categorieId","createdAt","duration","id","name","updateAt","url") SELECT "categorieId","createdAt","duration","id","name","updateAt","url" FROM "Answer";
DROP TABLE "Answer" CASCADE;
ALTER TABLE "_prisma_new_Answer" RENAME TO "Answer";
CREATE UNIQUE INDEX "Answer_name_key" ON "Answer"("name");
ALTER TABLE "Answer" ADD CONSTRAINT "Answer_categorieId_fkey" FOREIGN KEY ("categorieId") REFERENCES "Categories"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
CREATE TABLE "_prisma_new_User" (
    "id" SERIAL4 NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updateAt" TIMESTAMP(3) NOT NULL,
    "email" TEXT NOT NULL,
    "firstName" TEXT NOT NULL,
    "lastName" TEXT NOT NULL,
    "password" TEXT NOT NULL,

    CONSTRAINT "User_pkey" PRIMARY KEY ("id")
);
DROP INDEX "User_email_key";
INSERT INTO "_prisma_new_User" ("createdAt","email","firstName","id","lastName","password","updateAt") SELECT "createdAt","email","firstName","id","lastName","password","updateAt" FROM "User";
DROP TABLE "User" CASCADE;
ALTER TABLE "_prisma_new_User" RENAME TO "User";
CREATE UNIQUE INDEX "User_email_key" ON "User"("email");
CREATE TABLE "_prisma_new_Categories" (
    "id" SERIAL4 NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updateAt" TIMESTAMP(3) NOT NULL,
    "name" TEXT NOT NULL,

    CONSTRAINT "Categories_pkey" PRIMARY KEY ("id")
);
DROP INDEX "Categories_name_key";
INSERT INTO "_prisma_new_Categories" ("createdAt","id","name","updateAt") SELECT "createdAt","id","name","updateAt" FROM "Categories";
DROP TABLE "Categories" CASCADE;
ALTER TABLE "_prisma_new_Categories" RENAME TO "Categories";
CREATE UNIQUE INDEX "Categories_name_key" ON "Categories"("name");

-- CreateIndex
CREATE UNIQUE INDEX "_AnswerToQuestion_AB_unique" ON "_AnswerToQuestion"("A", "B");

-- CreateIndex
CREATE INDEX "_AnswerToQuestion_B_index" ON "_AnswerToQuestion"("B");

-- AddForeignKey
ALTER TABLE "_AnswerToQuestion" ADD FOREIGN KEY ("A") REFERENCES "Answer"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_AnswerToQuestion" ADD FOREIGN KEY ("B") REFERENCES "Question"("id") ON DELETE CASCADE ON UPDATE CASCADE;
