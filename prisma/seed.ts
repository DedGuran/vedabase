
import { PrismaClient } from "@prisma/client";
import bcrypt from 'bcrypt'
import {answersData} from "./answerData"

const prisma =new PrismaClient()

const run =async()=>{
    await Promise.all(answersData.map(async(answers)=>{
        return prisma.categories.upsert({
            where:{name:answers.name},
            update:{},
            create:{
                name:answers.name,
                answer:{
                create:answers.answers.map(answers=>({
                        name:answers.name,
                        duration: answers.duration,
                        url:answers.url,
                        description:answers.description
                    }))
                }
            }
        }) 
    })
    
    )
    
    const salt = bcrypt.genSaltSync()
    
    const user =await prisma.user.upsert({
        where:{email:"test@test.com"},
        update:{},
        create:{
            email:"test@test.com",
            password:bcrypt.hashSync("password",salt),
            firstName:"Andrey",
            lastName:"Gurlev"
        },

    })
    const answers = await prisma.answer.findMany({})
    await Promise.all(new Array(10).fill(1).map(async (_,i)=>{
        return prisma.question.create({
            data:{
                name:`Ответ ${i +1}?`,
                user:{
                    connect:{id:user.id}
                },
                answer:{
                    connect:answers.map((answers)=>({
                        id:answers.id
                    }))
                }
            }
        })
    }))
}

run().catch((e)=>{
    console.log(e)
    process.exit(1)
}).finally(async()=>{
    await prisma.$disconnect()
})