import { Box, Flex, Text } from "@chakra-ui/layout"
import Player from "./player"
import { useStoreState } from "easy-peasy"
import { useEffect, useRef, useState } from "react"
import React from "react"

const PlayerBar = () => {
    
    const answers = useStoreState((state: any) => state.activeAnswers)
    const activeAnswer = useStoreState((state: any) => state.activeAnswer)
    const activeAnswerRef=useRef(activeAnswer)
    
    useEffect(()=>{
        activeAnswerRef.current=activeAnswer
    },[activeAnswer])
    return (
        <Box height="100px" width="100vw" bg="gray.900">
            <Flex align="center">

                {activeAnswerRef.current ? <Box padding="20px" color="white" width="30%">
                    <Text fontSize="large">{activeAnswerRef.current.name}</Text>
                    {/*<Text fontSize="sm">{activeAnswer}</Text>*/}
                </Box> : null}

                <Box width="40%">
                    {activeAnswerRef.current ? (<Player answers={answers} activeAnswer={activeAnswerRef.current} />) : null}
                </Box>
            </Flex>

        </Box>
    )
}

export default PlayerBar