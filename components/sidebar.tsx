import NextImage from "next/image"
import NextLink from "next/link"
import {
    Box,
    List,
    ListItem,
    ListIcon,
    Divider,
    Center,
    LinkBox,
    LinkOverlay
} from "@chakra-ui/layout"
import {
    MdHome,
    MdSearch,
    MdLibraryMusic,
    MdPlaylistAdd,
    MdFavorite
} from "react-icons/md"
import { useQuestion } from "../lib/hooks"

const navMenu = [
    {
        name: "Главная",
        icon: MdHome,
        route: '/'
    },
    {
        name: "О проекте",
        icon: MdLibraryMusic,
        route: '/project'
    },
    {
        name: "А.Г. Хакимов",
        icon: MdLibraryMusic,
        route: '/athor'
    },
    {
        name: "Поиск",
        icon: MdSearch,
        route: '/search'
    },

]

const questionMenu = [
    {
        name: "Добавить ответ",
        icon: MdPlaylistAdd,
        route: '/'
    },
    {
        name: "Популярные",
        icon: MdFavorite,
        route: '/favorites'
    },
    {
        name: "Вопросы",
        icon: MdFavorite,
        route: '/questions'
    },
]
//const questions=new Array(30).fill(1).map((_,i)=>`Вопрос ${i+1}`)

const Sidebar = () => {
    const { questions } = useQuestion()

    return (
        <Box width="100%" height="calc(100vh - 100px)" bg="black" paddingX="5px" color="gray">
            <Box paddingY="20px" height="100%">
                <Box width="120px" marginBottom="20px" paddingX="20px">
                    <NextImage src="/logo.png" height={60} width={120} />
                </Box>
                <Box marginBottom="20px">
                    <List spacing={2}>
                        {navMenu.map(menu => (
                            <ListItem paddingX="20px" fontSize="16px" key={menu.name}>
                                <LinkBox>
                                    <NextLink href={menu.route} passHref>
                                        <LinkOverlay>
                                            <ListIcon as={menu.icon} color="white" marginRight="20px" />
                                            {menu.name}

                                        </LinkOverlay>
                                    </NextLink>
                                </LinkBox>
                            </ListItem>
                        ))}
                    </List>

                </Box>

                <Box marginTop="20px">
                    <List spacing={2}>
                        {questionMenu.map(menu => (
                            <ListItem paddingX="20px" fontSize="16px" key={menu.name}>
                                <LinkBox>
                                    <NextLink href={menu.route} passHref>
                                        <LinkOverlay>
                                            <ListIcon as={menu.icon} color="white" marginRight="20px" />
                                            {menu.name}

                                        </LinkOverlay>
                                    </NextLink>
                                </LinkBox>
                            </ListItem>
                        ))}
                    </List>
                </Box>
                <Divider color="gray.800" />
                <Box height="66%" overflowY="auto" paddingY="20px">
                    <List spacing={2}>
                        {questions.map((question) => (
                            <ListItem paddingX="20px" key={question.id}>
                                <LinkBox>
                                    <NextLink href={{
                                        pathname: "/questions/[id]",
                                        query: { id: question.id }
                                    }} passHref>
                                        <LinkOverlay>
                                            {question.name}
                                        </LinkOverlay>
                                    </NextLink>
                                </LinkBox>
                            </ListItem>
                        ))}
                    </List>
                </Box>
            </Box>
        </Box>
    )
}

export default Sidebar