import { ButtonGroup, Box, IconButton, RangeSlider, RangeSliderFilledTrack, RangeSliderTrack, RangeSliderThumb, Center, Flex, Text } from "@chakra-ui/react"
import ReactHowler from "react-howler"
import { useEffect, useRef, useState } from "react"
import { MdShuffle, MdSkipPrevious, MdSkipNext, MdOutlinePlayCircleFilled, MdOutlinePauseCircleFilled, MdOutlineRepeat } from "react-icons/md"
import { useStoreActions } from "easy-peasy"
import { formatTime } from "../lib/formatters"

const Player = ({ answers, activeAnswer }) => {
   
    const [playing, setPlaying] = useState(true)
    const [index, setIndex] = useState(
        
        answers.findIndex((s) =>  s.id === activeAnswer.id)
    )
    const [seek, setSeek] = useState(0.0)
    const [isSeeking, setIsSeeking] = useState(false)
    const [repeat, setRepeat] = useState(false)
    const [shuffle, setShuffle] = useState(false)
    const [duration, setDuration] = useState(0.0)
    const soundRef = useRef(null)
    const repeatRef=useRef(repeat)
    const setActiveAnswer = useStoreActions((state: any) => {
         
        return state.changeActiveAnswer
    })
    useEffect(() => {
        let timerId
        if (playing && !isSeeking) {
            const f = () => {
                setSeek(soundRef.current.seek())
                timerId = requestAnimationFrame(f)
            }
            timerId = requestAnimationFrame(f)
            return () => cancelAnimationFrame(timerId)
        }
        cancelAnimationFrame(timerId)
    }, [playing, isSeeking])

    useEffect(() => {
        setActiveAnswer(answers[index])
    }, [index, setActiveAnswer, answers])

    useEffect(()=>{
        repeatRef.current=repeat
    },[repeat])
    const setPlayState = (value) => {
        setPlaying(value)
    }
    const onShuffle = () => {
        setShuffle((state) => !state)
    }
    const onRepeat = () => {
        setRepeat((state) => !state)
    }
    const prevAnswer = () => {
        setIndex((state) => {
            return state ? state - 1 : answers.length - 1
        })
    }

    const nextAnswer = () => {
        setIndex((state) => {
            if (shuffle) {
                const next = Math.floor(Math.random() * answers.length)

                if (next === state) {
                    return nextAnswer()
                }
                return next
            } else {
                return state === answers.length - 1 ? 0 : state + 1
            }
        })
    }

    const onEnd = () => {
        if (repeatRef.current) {
            setSeek(0)
            soundRef.current.seek(0)
        } else {
            nextAnswer()
        }
    }
    const onLoad = () => {
        const answerDuration = soundRef.current.duration()
        setDuration(answerDuration)
    }
    const onSeek = (e) => {
        setSeek(parseFloat(e[0]))
        soundRef.current.seek(e[0])
    }
    return (
        <Box>
            <Box>
                <ReactHowler playing={playing} src={activeAnswer?.url} ref={soundRef} onLoad={onLoad} onEnd={onEnd} />
            </Box>
            <Center>
                <ButtonGroup>
                    <IconButton outline="none" variant="link" aria-label="shuffle" fontSize="24px" icon={<MdShuffle />} color={shuffle ? "white" : "gray.600"} onClick={onShuffle} />
                    <IconButton outline="none" variant="link" aria-label="skip" fontSize="24px" icon={<MdSkipPrevious />} onClick={prevAnswer} />
                    {playing ?
                        (<IconButton outline="none" variant="link" aria-label="pause" fontSize="40px" color="white" icon={<MdOutlinePauseCircleFilled />} onClick={() => { setPlayState(false) }} />)
                        : (<IconButton outline="none" variant="link" aria-label="play" fontSize="40px" color="white" icon={<MdOutlinePlayCircleFilled />} onClick={() => { setPlayState(true) }} />)}


                    <IconButton outline="none" variant="link" aria-label="next" fontSize="24px" icon={<MdSkipNext />} onClick={nextAnswer} />
                    <IconButton outline="none" variant="link" aria-label="repeat" fontSize="24px" icon={<MdOutlineRepeat />} color={repeat ? "white" : "gray.600"} onClick={onRepeat} />
                </ButtonGroup>
            </Center>
            <Box color="gray.600">
                <Flex justify="center" align="center">
                    <Box width="10%">
                        <Text fontSize="xs">{formatTime(seek)}</Text>
                    </Box>
                    <Box width="80%">
                        <RangeSlider
                            aria-label={["min", "max"]}
                            step={0.1}
                            min={0}
                            max={duration ? (duration.toFixed(2) as unknown as number) : 0}
                            id="player-range"
                            onChange={onSeek}
                            value={[seek]}
                            onChangeStart={() => setIsSeeking(true)}
                            onChangeEnd={() => setIsSeeking(false)}>
                            <RangeSliderTrack bg="gray.800">
                                <RangeSliderFilledTrack bg="gray.600" />
                            </RangeSliderTrack>
                            <RangeSliderThumb index={0} />
                        </RangeSlider>
                    </Box>
                    <Box width="10%" textAlign="right">
                        <Text fontSize="xs">{formatTime(duration)}</Text>
                    </Box>
                </Flex>

            </Box>

        </Box>

    )
}

export default Player