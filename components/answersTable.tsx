import React, { useEffect, useState } from "react";
import {Box} from "@chakra-ui/layout"
import {Table, Thead, Td, Tr,Tbody, Th, IconButton} from "@chakra-ui/react"
import {BsFillPlayFill} from "react-icons/bs"
import {AiFillClockCircle} from "react-icons/ai"
import { formatDate,formatTime } from "../lib/formatters"
import { useStoreActions } from "easy-peasy"


const AnswerTable=({answers})=>{
  
    const playAnswers=useStoreActions((store:any)=>store.changeActiveAnswers)
    const setActiveAnswer=useStoreActions((store:any)=>store.changeActiveAnswer)
    const handlePlay=(activeAnswer?)=>{
       
        setActiveAnswer(activeAnswer || answers[0])
        playAnswers(answers)
       
    }
     
    
  
    
    
    return (
        <Box bg="transparent" color="white">
            <Box padding="10px" marginBottom="20px">
                <Box marginBottom="30px">
                    <IconButton icon={<BsFillPlayFill fontSize="30px"/>} colorScheme="green" size="lg" isRound aria-label="play" onClick={()=>handlePlay()}/>
                    </Box>
                <Table variant="unstyled">
                    <Thead borderBottom="1px solid" borderColor="rgba(255,255,255,0.2)">
                        <Tr>
                            <Th>#</Th>
                            <Th>Title</Th>
                            <Th>Дата</Th>
                            <Th><AiFillClockCircle/></Th>
                        </Tr>
                    </Thead>
                    <Tbody>
                        {answers.map((answer,i)=>(
                            <Tr sx={{
                                transition:"all .3s",
                                "&:hover":{
                                    bg:"rgda(255,255,255,0.1)"
                                }
                            }}
                            key={answer.id}
                            cursor="pointer"
                            onClick={()=>handlePlay(answer)}>
                                <Td>
                                    {i+1}
                                </Td>
                                <Td>
                                    {answer.name}
                                </Td>
                                <Td>
                                    {formatDate(answer.createdAt)}
                                </Td>
                                <Td>
                                    {formatTime(answer.duration)}
                                </Td>
                            </Tr>
                        ))}
                    </Tbody>
                </Table>
            </Box>

        </Box>
    )
}

export default AnswerTable