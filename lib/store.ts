import {createStore,action} from "easy-peasy"

export const store = createStore({
    
    activeAnswers:[],
    activeAnswer:null,
    changeActiveAnswers:action((state:any,payload)=>{
        
        state.activeAnswers=payload
    }),
    changeActiveAnswer:action((state:any,payload)=>{
        
        state.activeAnswer=payload
    })
})