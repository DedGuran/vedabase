import formatDuration from "format-duration";

export const formatTime=(timeInSecondes=0)=>{
    return formatDuration(timeInSecondes*1000)
}

export const formatDate=(date: Date)=>{
   
    return new Date(date).toLocaleDateString("ru-RU",{
        year:"numeric",
        month:"short",
        day:"numeric"
    })

}