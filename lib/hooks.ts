import useSWR from "swr"
import fetcher from "./fetcher"

export const useMe = ()=>{
    const {data,error}=useSWR("/me",fetcher)

    return{
        user:data,
        isLoading:!data && !error,
        isError:error
    }
}

export const useQuestion = ()=>{
    const {data,error}=useSWR("/question",fetcher)
    
    return{
        questions:(data as any) || [],
        isLoading:!data && !error,
        isError:error
    }
}

export const useCategories = ()=>{
    const {data,error}=useSWR("/category",fetcher)

    return{
        categories:data,
        isLoading:!data && !error,
        isError:error
    }
}